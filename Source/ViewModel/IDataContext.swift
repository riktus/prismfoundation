//
//  IDataContext.swift
//  Pods-PrismFoundation_Tests
//
//  Created by Александр  Волков on 16.01.2019.
//

import Foundation

public protocol IDataContext {
    init()
}
