//
//  UIControlExtension.swift
//  Pods-PrismFoundation_Tests
//
//  Created by Александр  Волков on 16.01.2019.
//

import Foundation
import UIKit

public extension UIControl {
    
    public func bind(on event: UIControl.Event, to action: @escaping () -> Void) {
        let bindableClosure = BindableClosure(attachTo: self, closure: action)
        
        addTarget(bindableClosure, action: #selector(bindableClosure.invoke), for: event)
    }
}
