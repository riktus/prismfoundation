//
//  BindableBase.swift
//  Pods-PrismFoundation_Tests
//
//  Created by Александр  Волков on 16.01.2019.
//

import Foundation

@objcMembers
open class BindableBase : NSObject {
    
    private final var _bindings: [String: [() -> Void]] = [:]
    
    @discardableResult
    public func bind(on prop: String, to action: @escaping () -> Void) -> BindableBase {
        if _bindings[prop] == nil {
            _bindings[prop] = [action]
        } else {
            _bindings[prop]!.append(action)
        }
        
        return self
    }
    
    public final func raisePropertyChanged(prop: String) {
        if let actions = self._bindings[prop] {
            actions.forEach({ action in
                DispatchQueue.main.async {
                    action()
                }
            })
        }
    }
    
}
