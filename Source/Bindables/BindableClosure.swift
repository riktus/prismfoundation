//
//  BindableClosure.swift
//  Pods-PrismFoundation_Tests
//
//  Created by Александр  Волков on 16.01.2019.
//

import Foundation
import UIKit

private struct AssociatedKeys {
    static let bindableClosure = "bindableClosure"
}

public class BindableClosure {
    private var _closure: () -> Void
    
    init(attachTo: AnyObject, closure: @escaping () -> Void) {
        self._closure = closure
        
        objc_setAssociatedObject(attachTo, AssociatedKeys.bindableClosure, self, .OBJC_ASSOCIATION_RETAIN)
    }
    
    @objc public func invoke() {
        DispatchQueue.main.async { [weak self] in
            self?._closure()
        }
    }
}
