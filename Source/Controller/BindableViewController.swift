//
//  BindableViewController.swift
//  Pods-PrismFoundation_Tests
//
//  Created by Александр  Волков on 16.01.2019.
//

import Foundation
import UIKit

open class BindableViewController<T: IDataContext>: UIViewController {
    
    private var _dataContext: T?
    public var dataContext: T {
        get {
            if _dataContext == nil {
                _dataContext = T()
            }
            
            return _dataContext!
        }
        set {
            _dataContext = newValue
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
